import { auth, groupsRef, messagesRef, usersRef } from "../config/firebase";

const loginUser = (email, password) =>
  auth.signInWithEmailAndPassword(email, password)

const signupUser = (email, password) =>
  auth.createUserWithEmailAndPassword(email, password)

const createGroup = (members, name) => {
  let val = members.reduce((acc, curr) => {
    acc.members[curr] = true;
    return acc
  }, {
    name: name,
    members: {}
  })

  return groupsRef.add(val)
}

const getGroups = (userId, callback) => 
  groupsRef.where(`members.${userId}`, '==', true).onSnapshot(callback)

const getMessagesForGroup = (group, callback) => 
  messagesRef.where('groupId', '==', group.id).orderBy('timestamp').onSnapshot(callback)

const createNewMessage = (message) =>
  messagesRef.add(message)

const getUsers = (callback) =>
  usersRef.onSnapshot(callback)

const api = {
  loginUser,
  signupUser,
  createGroup,
  getGroups,
  createNewMessage,
  getMessagesForGroup,
  getUsers
}

export default api;