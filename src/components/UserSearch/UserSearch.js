import React from 'react';
import {
  ChatListCol,
  UserInfoHeader,
  UserList,
  StyledButtonIcon
} from '../ChatHome/styles';
import { Button } from 'rmwc/Button';
import { ListItem, ListItemText } from 'rmwc/List';

const UserSearch = (props) => (
  <ChatListCol>
    <UserInfoHeader>
      <Button onClick={props.onClickBack}>
        <StyledButtonIcon strategy="ligature" use="arrow_back"/>
      </Button>
      Select a user to chat with
    </UserInfoHeader>
    <UserList>
      {props.userList.map((user, index) =>
        <ListItem key={index} onClick={() => props.onClickUser(user)}>
          <ListItemText>{user.email}</ListItemText>
        </ListItem>
      )}
    </UserList>
  </ChatListCol>
);

 
export default UserSearch;