import styled from 'styled-components';

export const AppLogo = styled.img`
  height: 40px;
`
export const AppName = styled.span`
  font-size: 14px;
  margin-top: 11px;
  color: #FFF;
  margin-left: 8px;
`

export const AppHeader = styled.div`
  display: -webkit-flex;
  display: flex;
  align-items: flex-start;
  height: 55vh;
  background: #1c272b;
  padding: 24px;
  background-image: url('https://www.getpostman.com/img/v2/homepage/background-stars.svg');
  background-position: center center;
  background-size: cover;
`
export const ViewPort = styled.div`
  display: -webkit-flex;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
`