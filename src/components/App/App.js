import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Page } from '../../styles/globalStyles';
import LoginPage from '../LoginPage';
import { auth } from '../../config/firebase';
import ChatHome from '../ChatHome';
import { AppHeader, ViewPort, AppLogo, AppName } from './styles';
import { loginUser, signupUser, getAuthState } from '../../redux/actions/auth';
import { Snackbar } from 'rmwc/Snackbar';

import logo from '../../assets/logo.svg';
import LoaderCard from '../LoaderCard';
import { connect } from 'react-redux';

class App extends Component {
  state = {
    loading: true,
    user: null
  }

  constructor() {
    super();
    this.state = {
      showSnackBar: false,
      snackbarMessage: ''
    }

    // Bind Functions
    this.renderHomePage = this.renderHomePage.bind(this);
    this.loginUser = this.loginUser.bind(this)
    this.signupUser = this.signupUser.bind(this)
    this.hideSnackbar = this.hideSnackbar.bind(this)
  }

  componentWillMount() {
    this.props.dispatch(getAuthState())
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.error) {
      this.setState({
        showSnackBar: true,
        snackbarMessage: nextProps.auth.error.message
      })
    }
  }

  hideSnackbar() {
    this.setState({ showSnackBar: false })
  }

  loginUser(email, password) {
    this.props.dispatch(loginUser(email, password))
  }

  signupUser(email, password) {
    this.props.dispatch(signupUser(email, password))
  }

  renderHomePage() {
    return this.props.auth.user ?
      <ChatHome user={this.props.auth.user} /> : <LoginPage auth={auth} onLoginUser={this.loginUser} onSignupUser={this.signupUser} />;
  }

  render() {
    return (
      <Page>
        <AppHeader>
          <AppLogo src={logo} />
          <AppName>| CHAT</AppName>
        </AppHeader>
        { this.props.auth.intializing ? (
          <ViewPort>
            <LoaderCard />
          </ViewPort>
        ) : (
          <Router>
            <ViewPort>
              <Route exact path='/' render={this.renderHomePage} />
            </ViewPort>          
          </Router>
        )}
        <Snackbar
          onHide={this.hideSnackbar}
          show={this.state.showSnackBar}
          message={this.state.snackbarMessage}
        />
      </Page>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth
})

export default connect(mapStateToProps)(App);
