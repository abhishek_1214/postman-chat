import React, { Component } from 'react';
import loader from '../../assets/loader.gif'
import { Card, LoaderImg, LoaderText } from './styles';

class LoaderCard extends Component {
  state = {}
  render() { 
    return (
      <Card z={4}>
        <LoaderImg src={loader} />
        <LoaderText>Setting things up...</LoaderText>
      </Card>
    )
  }
}
 
export default LoaderCard;