import styled from 'styled-components';
import { Elevation } from 'rmwc/Elevation';

export const Card = styled(Elevation)`
  display: -webkit-flex;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 20vw;
  height: 30vh;
  padding: 24px;
  background: #fff;
`

export const LoaderImg = styled.img`
  height: 72px;
  width: 72px;
`

export const LoaderText = styled.p`
  font-size: 16px;
  font-weight: 500;
  color: #333;
  margin-top: 24px;
`