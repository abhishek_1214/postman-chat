import React, { Component } from 'react';
import { 
  ChatContainer,
  ChatHeader,
  ChatGroupName,
  MessagesList,
  ChatInputContainer,
  ChatInput,
  SentChatBubble,
  RecievedChatBubble
} from './styles';
import { Button, ButtonIcon } from 'rmwc';

const EmptyState = () => (
  <ChatContainer>
    Pick a chat
  </ChatContainer>
)

class Chat extends Component {
  constructor() {
    super()
    this.state = {
      messageInput: '',
    }

    // Bind Functions
    this.updateMessage = this.updateMessage.bind(this)
    this.onClickSend = this.onClickSend.bind(this)
  }

  onClickSend() {
    if (this.state.messageInput) {
      this.props.onSendMessage(this.state.messageInput)
      this.setState({ messageInput: '' })
    }
  }

  updateMessage(event) {
    this.setState({ messageInput: event.target.value })
  }

  render() {
    return (
      <ChatContainer>
        {this.props.activeGroup.id ? (
          <ChatContainer>
            <ChatHeader>
              <ChatGroupName>Chat</ChatGroupName>
            </ChatHeader>
            <MessagesList>
              {this.props.messagesList.map(item =>
                this.props.currentUser.uid === item.from ?
                  <SentChatBubble>{item.message}</SentChatBubble> : <RecievedChatBubble>{item.message}</RecievedChatBubble>
              )}
            </MessagesList>
            <ChatInputContainer>
              <ChatInput value={this.state.messageInput} onChange={this.updateMessage} placeholder='Send a message...' />
              <Button onClick={this.onClickSend}>
                <ButtonIcon strategy="ligature" use="send" />
              </Button>
            </ChatInputContainer>
          </ChatContainer>
        ) : <EmptyState />}
      </ChatContainer>
    )
  }
}

export default Chat;