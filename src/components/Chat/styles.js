import styled from 'styled-components';

export const ChatContainer = styled.div`
  display: -webkit-flex;
  display: flex;
  flex: 1;
  background: #dedede;
  flex-direction: column;
`

export const ChatHeader = styled.div`
  display: -webkit-flex;
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 64px;
  padding: 0 16px;
  background: #fff;
  box-shadow: 2px 2px 4px rgba(0,0,0,0.12);
`

export const ChatGroupName = styled.h4`
  color: #ff6c37;
  font-size: 16px;
  font-weight: 500;
`

export const MessagesList = styled.div`
  display: -webkit-flex;
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: 8px 0;
  align-items: flex-start;
  background: #dedede;
`

export const ChatInputContainer = styled.div`
  display: -webkit-flex;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 16px;
  background: #fff;
`

export const ChatInput = styled.input`
  border-radius: 2px;
  padding: 12px 16px;
  border: none;
  background: #c3c3c3;
  flex: 1;
`

export const RecievedChatBubble = styled.div`
  flex: 0;
  flex-direction: column;
  font-size: 13px;
  padding: 8px 16px;
  border-radius: 2px;
  background: #fff;
  margin: 8px;
  align-self: flex-start;
`

export const SentChatBubble = styled.div`
  flex: 0;
  flex-direction: column;
  font-size: 13px;
  padding: 8px 16px;
  border-radius: 2px;
  background: #fbd4c6;
  margin: 8px;
  align-self: flex-end;
`