import React, { Component } from 'react';
import { 
  ContainerCard,
} from './styles';
import 'firebase/auth';
import UserSearch from '../UserSearch';
import ChatList from '../ChatList';
import { connect } from 'react-redux';
import { createGroup, getGroups } from '../../redux/actions/groups';
import { createMessage } from '../../redux/actions/messages';
import { getUsers } from '../../redux/actions/users';
import Chat from '../Chat/Chat';


class ChatHome extends Component {

  constructor() {
    super();
    this.state = {
      searchUserList: [],
      showSearchList: false,
      activeGroup: { id: null },
    }

    // Bind Functions
    this.openUserSearch = this.openUserSearch.bind(this)
    this.closeUserSearch = this.closeUserSearch.bind(this)
    this.createNewChat = this.createNewChat.bind(this)
    this.setActiveGroup = this.setActiveGroup.bind(this)
    this.createNewMessage = this.createNewMessage.bind(this)
  }

  componentDidMount() {
    this.props.dispatch(getUsers())
    this.props.dispatch(getGroups(this.props.user.uid))
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.userState.users.length > 0) {
      let searchList = nextProps.userState.users;
      searchList = searchList.filter(item => item.uid !== this.props.user.uid)

      if (nextProps.groups.groups.length > 0) {
        nextProps.groups.groups.forEach(group => {
          Object.keys(group.members).forEach(member => {
            searchList = searchList.filter(item => item.uid !== member)
          })
        })
      }

      this.setState({ searchUserList: searchList })
    }
  }


  closeUserSearch() {
    this.setState({ showSearchList: false })
  }

  setActiveGroup(group) {
    if(group) {
      this.setState({ activeGroup: group })
    }
  }

  createNewMessage(message) {
    this.props.dispatch(createMessage({
      message: message,
      groupId: this.state.activeGroup.id,
      from: this.props.user.uid,
      timestamp: new Date().toString()
    }))
  }

  createNewChat(user) {
    this.props.dispatch(createGroup(
      [user.uid, this.props.user.uid],
      user.email
    ))
    this.setState({ showSearchList: false })
  }
  
  openUserSearch() {
    this.setState({ 
      activeGroup: {id: null},
      showSearchList: true 
    })
  }

  render() { 
    return (
      <ContainerCard z={4}>
        {this.state.showSearchList ? (
          <UserSearch 
            onClickUser={this.createNewChat}
            userList={this.state.searchUserList} 
            onClickBack={this.closeUserSearch}
          />
        ) : (
          <ChatList
            activeGroup={this.state.activeGroup}
            onChatGroupClick={this.setActiveGroup}
            groups={this.props.groups.groups}
            user={this.props.user} 
            onClickNewChat={this.openUserSearch} 
          />
        )}
        <Chat
          currentUser={this.props.user}
          messagesList={
            this.props.messages.messages[this.state.activeGroup.id] ?
              this.props.messages.messages[this.state.activeGroup.id] : []
          }
          onSendMessage={this.createNewMessage}
          activeGroup={this.state.activeGroup} 
        />
      </ContainerCard>
    )
  }
}

const mapStateToProps = state => ({
  userState: state.users,
  groups: state.groups,
  messages: state.messages
})

export default connect(mapStateToProps)(ChatHome);