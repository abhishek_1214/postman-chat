import styled from 'styled-components';
import { Elevation } from 'rmwc/Elevation';
import { List, ListItem, ListItemText } from 'rmwc/List';
import { Icon } from 'rmwc/Icon'
import { ButtonIcon } from 'rmwc/Button';

export const ContainerCard = styled(Elevation)`
  display: -webkit-flex;
  display: flex;
  flex-direction: row;
  height: 80vh;
  width: 65vw;
  background: #fff;
  border-radius: 2px;
`

export const ChatListCol = styled.div`
  flex-direction: column;
  width: 35%;
  border-right: 2px solid #dedede;
  background: #fafafa;
`

export const UserInfoHeader = styled.div`
  display: -webkit-flex;
  display: flex;
  background: #FFF;
  align-items: center;
  padding: 0 16px;
  height: 64px;
  flex-direction: row;
  border-bottom: 1px solid #c3c3c3;
`
export const UserAvatar = styled.div`
  display: -webkit-flex;
  display: flex;
  background: #ff6c37;
  align-items: center;
  justify-content: center;
  height: 36px;
  width: 36px;
  border-radius: 50%;
  margin-right: 16px;
  color: #fff;
  font-weight: 800;
`

export const UserEmail = styled.p`
  font-size: 16px;
`

export const UserList = styled(List)`
  background: #fafafa;
`;

export const UserListItem = styled(ListItem)``

export const ActiveUserListItem = styled(ListItem)`
  background: #dedede;
`

export const ActionListText = styled(ListItemText)`
  color: #ff6c37;
  font-weight: 500;
`

export const UserListText = styled(ListItemText)`
  font-weight: 500;
`

export const StyledIcon = styled(Icon)`
  color: #ff6c37;
`

export const StyledButtonIcon = styled(ButtonIcon)`
  color: #ff6c37;
`