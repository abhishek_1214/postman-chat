import styled from 'styled-components';
import { Elevation } from 'rmwc/Elevation';
import { Button } from 'rmwc/Button';

export const LoginCard = styled(Elevation)`
  display: -webkit-flex;
  display: flex;
  flex-direction: column;
  width: 20vw;
  padding: 24px;
  background: #fff;
`

export const LoginButton = styled(Button)`
  margin-top: 24px;
`

export const LoginText = styled.a`
  margin: 16px 0;
  text-align: center;
  font-size: 13px;
  text-decoration: none;
`