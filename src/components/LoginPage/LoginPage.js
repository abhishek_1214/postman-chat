import React, { Component } from 'react';
import { LoginCard, LoginButton, LoginText } from './styles';
import { TextField } from 'rmwc/TextField';

class LoginPage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      showSignup: true
    }

    this.authWithEmailandPassword = this.authWithEmailandPassword.bind(this)
    this.toggleSignupButton = this.toggleSignupButton.bind(this)
    this.signupWithEmailPassword = this.signupWithEmailPassword.bind(this)
  }

  authWithEmailandPassword() {
    let email = this.emailInput.value;
    let password = this.passwordInput.value;

    if (email && password) {
      this.props.onLoginUser(email, password)
    }
  }

  signupWithEmailPassword() {
    let email = this.emailInput.value;
    let password = this.passwordInput.value;

    if (email && password) {
      this.props.onSignupUser(email, password)
    }
  }

  toggleSignupButton() {
    this.setState({ showSignup: !this.state.showSignup })
  }

  render() { 
    return (
      <LoginCard z={4}>
        Signup and begin chatting...
        <TextField required ref={r => this.emailInput = r} label='Email' type='email' />
        <TextField required ref={r => this.passwordInput = r} label='Password' type='password' />
        {this.state.showSignup ? 
          <LoginButton onClick={this.signupWithEmailPassword} raised>Signup</LoginButton>
        :
          <LoginButton onClick={this.authWithEmailandPassword} raised>Login</LoginButton>
        }
        <LoginText onClick={this.toggleSignupButton} href={'#'}>
          {this.state.showSignup ? 'Login with existing account' : 'Signup for new account' }
        </LoginText>
      </LoginCard>
    );
  }
}

export default LoginPage;