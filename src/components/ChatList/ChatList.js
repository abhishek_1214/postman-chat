import React from 'react';
import { ListItemMeta } from 'rmwc/List';
import  {
  ChatListCol,
  UserInfoHeader,
  UserAvatar,
  UserEmail,
  UserList,
  ActionListText,
  StyledIcon,
  UserListItem,
  UserListText,
  ActiveUserListItem
} from '../ChatHome/styles';

const ChatList = (props) => (
  <ChatListCol>
    <UserInfoHeader>
      <UserAvatar>
        {props.user.email[0].toUpperCase()}
      </UserAvatar>
      <UserEmail>{props.user.email}</UserEmail>
    </UserInfoHeader>
    <UserList>
      {props.groups.map((group, index) =>
        group.id === props.activeGroup.id ? (
          <ActiveUserListItem key={index} onClick={() => props.onChatGroupClick(group)}>
            <UserListText>{group.name}</UserListText>
          </ActiveUserListItem>
        ) : (
          <UserListItem key={index} onClick={() => props.onChatGroupClick(group)}>
            <UserListText>{group.name}</UserListText>
          </UserListItem>
      ))}
      <UserListItem onClick={props.onClickNewChat}>
        <ActionListText>Start a new Chat</ActionListText>
        <ListItemMeta>
          <StyledIcon strategy="ligature" use="add" />
        </ListItemMeta>
      </UserListItem>
    </UserList>
  </ChatListCol>
);

export default ChatList;