import { GET_USERS_RECIEVE } from "../actions/users";

const initial = {
  users: [],
  loading: false,
  error: null
}

const usersReducer = (state=initial, action) => {
  switch (action.type) {
    case GET_USERS_RECIEVE:
      return {
        ...state,
        users: action.payload.users
      }
    default:
      return state;
  }
}

export default usersReducer;