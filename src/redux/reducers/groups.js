import { GET_GROUPS_RECIEVE } from '../actions/groups';

const initial = {
  groups: [],
  loading: false,
  error: null
}

const groupsReducer = (state = initial, action) => {
  switch (action.type) {
    case GET_GROUPS_RECIEVE:
      return {
        ...state,
        groups: action.payload.groups
      }
    default:
      return state;
  }
}

export default groupsReducer;