import { UPDATE_AUTH_STATE, INITIALIZE_AUTH, LOGIN_USER_ERROR, SIGNUP_USER_ERROR, LOGIN_USER_RECIEVE, SIGNUP_USER_RECIEVE } from "../actions/auth";

const initial = {
  intializing: false,
  user: null,
  loggedIn: false,
  loading: false,
  error: false
}

const authReducer = (state = initial, action) => {
  switch (action.type) {
    case INITIALIZE_AUTH:
      return {
        ...state,
        intializing: true
      }
    case UPDATE_AUTH_STATE:
      return {
        ...state,
        user: action.payload.user,
        intializing: false
      }
    case LOGIN_USER_RECIEVE:
      return {
        ...state,
        user: action.payload.user,
        error: null
      }
    case SIGNUP_USER_RECIEVE:
      return {
        ...state,
        user: action.payload.user,
        error: null
      }
    case LOGIN_USER_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      }

    case SIGNUP_USER_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      }
    default:
      return state;
  }
}

export default authReducer;