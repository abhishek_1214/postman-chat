import { GET_MESSAGES_RECIEVE } from "../actions/messages";

const initial = {
  messages: {},
  loading: false,
  error: null
}

const messagesReducer = (state = initial, action) => {
  switch (action.type) {
    case GET_MESSAGES_RECIEVE:
      return {
        ...state,
        messages: {
          ...state.messages,
          [action.payload.groupId]: action.payload.messages
        }
      };
    default:
      return state;
  }
}

export default messagesReducer;