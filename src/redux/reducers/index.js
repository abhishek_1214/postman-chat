import { combineReducers } from 'redux';
import usersReducer from './users';
import groupsReducer from './groups';
import messagesReducer from './messages';
import authReducer from './auth';

export const rootReducer = combineReducers({
  auth: authReducer,
  users: usersReducer,
  groups: groupsReducer,
  messages: messagesReducer
})