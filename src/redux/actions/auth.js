import api from '../../api';
import { auth } from '../../config/firebase';

export const INITIALIZE_AUTH = 'INITIALIZE_AUTH';
export const LOGIN_USER_REQUEST = 'LOGIN_USER_REQUEST';
export const LOGIN_USER_RECIEVE = 'LOGIN_USER_RECIEVE';
export const LOGIN_USER_ERROR = 'LOGIN_USER_ERROR'
export const SIGNUP_USER_REQUEST = 'SIGNUP_USER_REQUEST';
export const SIGNUP_USER_RECIEVE = 'SIGNUP_USER_RECIEVE';
export const SIGNUP_USER_ERROR = 'SIGNUP_USER_ERROR';
export const UPDATE_AUTH_STATE = 'UPDATE_AUTH_STATE';

export const loginUserRequest = () => ({
  type: LOGIN_USER_REQUEST
})

export const loginUserRecieve = (user) => ({
  type: LOGIN_USER_RECIEVE,
  payload: {
    user
  }
})

export const loginUserError = (error) => ({
  type: LOGIN_USER_RECIEVE,
  payload: {
    error
  }
})

export const signupUserRequest = () => ({
  type: SIGNUP_USER_REQUEST
})

export const signupUserRecieve = (user) => ({
  type: SIGNUP_USER_RECIEVE,
  payload: {
    user
  }
})

export const signupUserError = (error) => ({
  type: SIGNUP_USER_ERROR,
  payload: {
    error
  }
})

export const updateAuthState = (user) => ({
  type: UPDATE_AUTH_STATE,
  payload: {
    user
  }
})

export const intializeAuth = () => ({
  type: INITIALIZE_AUTH
})

export const loginUser = (email, password) =>
  async dispatch => {
    dispatch(loginUserRequest())
    api.loginUser(email, password)
      .then(res => dispatch(loginUserRecieve(res.user.toJSON())))
      .catch(err => dispatch(loginUserError(err)))
  }

export const signupUser = (email, password) =>
  async dispatch => {
    dispatch(signupUserRequest())
    api.signupUser(email, password)
      .then(res => dispatch(signupUserRecieve(res.user.toJSON())))
      .catch(err => dispatch(signupUserError(err)))
  }

export const getAuthState = () =>
  async dispatch => {
    dispatch(intializeAuth())
    auth.onAuthStateChanged(user => {
      user = user ? user.toJSON() : null;
      dispatch(updateAuthState(user))
    })
  }