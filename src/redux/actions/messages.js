import api from "../../api";

export const CREATE_MESSAGE_REQUEST = 'CREATE_MESSAGE_REQUEST';
export const CREATE_MESSAGE_RECIEVE = 'CREATE_MESSAGE_RECIEVE';
export const CREATE_MESSAGE_ERROR = 'CREATE_MESSAGE_ERROR';
export const GET_MESSAGES_REQUEST = 'GET_MESSAGES_REQUEST';
export const GET_MESSAGES_RECIEVE = 'GET_MESSAGES_RECIEVE';
export const GET_MESSAGES_ERROR = 'GET_MESSAGES_ERROR';

export const createMessageRequest = () => ({
  type: CREATE_MESSAGE_REQUEST
})

export const createMessageRecieve = (response) => ({
  type: CREATE_MESSAGE_RECIEVE,
  payload: {
    response
  }
})

export const createMessageError = (error) => ({
  type: CREATE_MESSAGE_ERROR,
  payload: {
    error
  }
})

export const getMessagesRequest = () => ({
  type: GET_MESSAGES_REQUEST
})

export const getMessagesRecieve = (groupId, messages) => ({
  type: GET_MESSAGES_RECIEVE,
  payload: {
    groupId,
    messages
  }
})

export const getMessagesError = (error) => ({
  type: GET_MESSAGES_ERROR,
  payload: {
    error
  }
})

export const createMessage = (message) =>
  async dispatch => {
    api.createNewMessage(message)
      .then(res => dispatch(createMessageRecieve(res)))
      .catch(err => dispatch(createMessageError(err)))
  }

export const getMessages = (groups) =>
  async dispatch => {
    groups.forEach(group => {
      api.getMessagesForGroup(group, snapshot => {
        if (snapshot.size > 0) {
          let messages = [];
          snapshot.forEach(doc => messages.push({
            id: doc.id,
            ...doc.data()
          }))
          dispatch(getMessagesRecieve(group.id, messages))
        }
      })
    })
  }