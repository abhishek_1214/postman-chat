import api from '../../api'
import { getMessages } from "./messages";

export const CREATE_GROUP_REQUEST = 'CREATE_GROUP_REQUEST';
export const CREATE_GROUP_RECIEVE = 'CREATE_GROUP_RECIEVE';
export const CREATE_GROUP_ERROR = 'CREATE_GROUP_ERROR';
export const GET_GROUPS_REQUEST = 'GET_GROUPS_REQUEST';
export const GET_GROUPS_RECIEVE = 'GET_GROUPS_RECIEVE';
export const GET_GROUPS_ERROR = 'GET_GROUPS_ERROR';


export const createGroupRequest = () => ({
  type: CREATE_GROUP_REQUEST
})

export const createGroupRecieve = (response) => ({
  type: CREATE_GROUP_RECIEVE,
  payload: {
    response
  }
})

export const createGroupError = (error) => ({
  type: CREATE_GROUP_ERROR,
  payload: {
    error
  }
})

export const getGroupsRequest = () => ({
  type: GET_GROUPS_REQUEST,
})

export const getGroupsRecieve = (groups) => ({
  type: GET_GROUPS_RECIEVE,
  payload: {
    groups
  }
})

export const getGroupsError = (error) => ({
  type: GET_GROUPS_ERROR,
  payload: {
    error
  }
})

export const createGroup = (members, name) =>
  async dispatch => {
    dispatch(createGroupRequest())
    api.createGroup(members, name)
      .then(res => dispatch(createGroupRecieve(res)))
      .catch(err => dispatch(createGroupError(err)))
  }

export const getGroups = (userId) =>
  async dispatch => {
    dispatch(getGroupsRequest())
    api.getGroups(userId, snapShot => {
        let groups = [];
        snapShot.forEach(doc => groups.push({
          id: doc.id,
          ...doc.data()
        }))
        dispatch(getMessages(groups))
        dispatch(getGroupsRecieve(groups)) 
      })
  }