import api from "../../api";

export const GET_USERS_REQUEST = 'GET_USERS_REQUEST';
export const GET_USERS_RECIEVE = 'GET_USERS_RECIEVE';
export const GET_USERS_ERROR = 'GET_USERS_ERROR';

export const getUsersRequest = () => ({
  type: GET_USERS_REQUEST
})

export const getUsersRecieve = (users) => ({
  type: GET_USERS_RECIEVE,
  payload: {
    users
  }
})

export const getUsersError = (error) => ({
  type: GET_USERS_ERROR,
  payload: {
    error
  }
})

export const getUsers = () =>
  async dispatch => {
    dispatch(getUsersRequest())
    api.getUsers(snapShot => {
      let users = [];
      snapShot.forEach(doc => users.push(doc.data()))
      dispatch(getUsersRecieve(users))
    })
  }