import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import registerServiceWorker from './registerServiceWorker';
import { injectGlobal } from 'styled-components';

import '../node_modules/material-components-web/dist/material-components-web.min.css';
import redux from './redux/store';

injectGlobal`
  @import 'https://fonts.googleapis.com/icon?family=Material+Icons';
  
  body {
    margin: 0;
    padding: 0;
    font-family: sans-serif;
  }

  p, h1, h2, h3, h4, h5, h6 {
    margin: 0;
    padding: 0;
  }
`

ReactDOM.render(
  <Provider store={redux.store}>
    <PersistGate loading={null} persistor={redux.persistor}>
      <App />
    </PersistGate>
  </Provider>
, document.getElementById('root'));

registerServiceWorker();
