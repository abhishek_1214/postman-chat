import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const config = {
  apiKey: "AIzaSyBZ4oGYgGTV2WZq4qEY0HcZh0BR9i14u1Q",
  authDomain: "postman-chat.firebaseapp.com",
  databaseURL: "https://postman-chat.firebaseio.com",
  projectId: "postman-chat",
  storageBucket: "postman-chat.appspot.com",
  messagingSenderId: "807260316840"
}

export const app = firebase.initializeApp(config)
export const auth = app.auth()
export const db = app.firestore()

db.settings({timestampsInSnapshots: true});

// Refs to Firestore collections
export const usersRef = db.collection('/users');
export const groupsRef = db.collection('/groups');
export const messagesRef = db.collection('/messages');