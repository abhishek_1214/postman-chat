import styled from 'styled-components';

export const Page = styled.div`
  display: -webkit-flex;
  display: flex;
  flex-direction: column;
  background: #dedede;
  min-height: 100vh;
`;

export const PageContent = styled.div`
  display: -webkit-flex;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  flex: 1;
`